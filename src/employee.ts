// employee.ts
/**
 * Employee controller.
 * 
 * @module
 */

/** Employee structure */
interface Employee
{
/**
 * @template T Expected detail type
 * @param key The empoyee detail to obtain
 * @returns The contents of the / all requested employee detail(s) if found, empty cell if not
 */
  Get<T extends Cell | Map<string, Cell> = Cell>(key?: string): T;

  /**
   * @param detailKey The employee detail to obtain
   * @param value Its new value
   * @returns True if it was successfully set
   */
  Set(detailKey: string, value?: CellValue): boolean;

  /** The employee's ID */
  ID: CellValue;

  /** The employee's name */
  Name: string;

  /** The employee's working days, presented as an array of integers between 0-6 */
  WorkingDays: number;
  
  /** True if the employee works holidays */
  WorkingHolidays: boolean;

  /** Preferred shift start time */
  PrefStart: Date;

  /** Preferred shift end time */
  PrefEnd: Date;

  /** The ID for the calender, which holds their schedule's calendar events */
  CalendarID?: string;

  /** The employee's active schedule */
  Schedule?: Schedule;
}

/** Product class for the creation of Employee objects */
class EmployeeProduct implements Employee
{
  public Get: <T extends Cell | Map<string, Cell> = Cell>(key?: string) => T;
  public Set: (detailKey: string, value: CellValue) => boolean;
  public ID: CellValue;
  public Name: string;
  public WorkingDays: number;
  public WorkingHolidays: boolean;
  public PrefStart: Date;
  public PrefEnd: Date;
  public Schedule?: Schedule;
  private calendarId: string;

  public get CalendarID() { return this.calendarId; };
  public set CalendarID(x)
  {
    this.calendarId = x;
    this.Set("CalendarID", x);
  };

  constructor(get: <T extends Cell | Map<string, Cell> = Cell>(key?: string) => T,
              set: (detailKey: string, value: CellValue) => boolean,
              id: CellValue, name: string, workingDays: number, workingHolidays: boolean, prefStart: Date, prefEnd: Date, calendarId: string)
  {
    this.Get = get;
    this.Set = set;
    this.ID = id;
    this.Name = name;
    this.WorkingDays = workingDays;
    this.WorkingHolidays = workingHolidays;
    this.PrefStart = prefStart;
    this.PrefEnd = prefEnd;
    this.calendarId = calendarId;
  }
}

/** Structure used for defining an employee's working days via bitwise flags */
enum WorkingDays
{
  NONE,
  MON = 1,
  TUE = 1 << 1,
  WED = 1 << 2,
  THU = 1 << 3,
  FRI = 1 << 4,
  SAT = 1 << 5,
  SUN = 1 << 6,
  ALL = ~(~0 << 7),
}

/** [Builder](https://en.wikipedia.org/wiki/Builder_pattern) superclass for creating Employee models */
class EmployeeBuilder
{
  private product?: EmployeeProduct;
  private details?: Map<string, Cell>;

  public Build(employeeDetails: Map<string, Cell>): EmployeeProduct
  {
    this.details = new Map<string, Cell>(employeeDetails);

    var id =              this.GetDetails("ID")?.Val();
    var name =            this.GetDetails("Name").Val<string>();
    var workingDays =     this.parseWorkingDays().Val<number>();
    var workingHolidays = this.GetDetails("Working holidays").Val<boolean>();
    var prefStart =       this.GetDetails("Pref. start").Val<Date>();
    var prefEnd =         this.GetDetails("Pref. end").Val<Date>();
    var calendarId =      this.GetDetails("CalendarID").Val<string>();

    this.product = new EmployeeProduct(this.GetDetails, this.SetDetail, id, name, workingDays, workingHolidays, prefStart, prefEnd, calendarId);
    this.product.Schedule = new Schedule(this.product);;
    
    return this.product;
  }

  /**
   * @template T Expected detail type
   * @param key The empoyee detail to obtain
   * @returns The contents of the / all requested employee detail(s) if found, empty string if not
   */
  private GetDetails = <T extends Cell | Map<string, Cell> = Cell>(key?: string): T => key === undefined ? <T><unknown>this.details : <T><unknown>this.details?.get(key) ?? Cell.Blank();

  /**
   * @param detailKey The employee detail to obtain
   * @param value Its new value
   * @returns True if it was successfully set
   */
  private SetDetail(detailKey: string, value: CellValue): boolean
  {
    var detail = this.details?.get(detailKey);
    var [row, column] = [detail?.Row, detail?.Column];
    var valueCell: Cell;

    if(row == undefined || column == undefined)
    {
      console.log(this.constructor.name + "(): Can't find detail with key '" + detailKey + "'");
      return false;
    }

    if(value == undefined)
    {
      console.log(this.constructor.name + "(): Invalid value for '" + detailKey + "'");
      return false;
    }

    valueCell = new Cell(value, row, column);

    this.details?.set(detailKey, valueCell);

    Roster.Singleton.UpdateEmployeeDetail(valueCell);

    return true;
  }

  /** Parse the `Mon,Tue,Wed` string from the employee details into a format suitable for comparisson with Date().getDay() */
  private parseWorkingDays(): Cell<number>
  {
    var wd: Cell<string> = this.GetDetails("Working days");
    var wdStrSplit: string[] = wd == undefined ? new Array<string>("") : wd.Val<string>().split(",");
    var workingDays: WorkingDays = WorkingDays.NONE;
    var workingDaysCell: Cell<number>;

    for(var wdIdx in wdStrSplit)
    {
      var wdStr: string = wdStrSplit[wdIdx];

      ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'].forEach((x, i) => wdStr == x ? workingDays |= i : false);

      if(workingDays == WorkingDays.NONE)
        console.log(this.constructor.name + "(): Unrecognised working days '" + wd + "' found at [" + wd.Row + ", " + wd.Column + "]");
    }

    workingDaysCell = new Cell<number>(workingDays, wd.Row, wd.Column);

    return workingDaysCell;
  }
}