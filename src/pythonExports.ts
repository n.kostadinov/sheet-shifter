// pythonExports.ts
/**
 * Functions designed to invoke specific functionalities, for testing purposes.
 * 
 * These functions are intended to be called from the Python Testing Module via the Apps Script API.
 * 
 * These functions can neither take parameters, nor return values, which are of Apps Script-specific object types
 * (such as a Document or a Calendar); they can only take and return primitive types such as string, number, array, object, or boolean.
 * 
 * @module
 */

// https://stackoverflow.com/questions/15682346/how-to-unit-test-google-apps-scripts

/**
 * Load the roster and retrieve the contents of a detail, associated to an employee.
 * 
 * @param id The employee's ID
 * @param detail The employee detail (e.g 'Name', 'Working days', 'Pref. start' ...)
 * @returns The contents of the requested employee detail
 */
function py_getEmployeeDetails(id: CellValue, detail: string): any
{
  var ret: any;

  var entry: Main = entryPoint();
  var employee = entry.Roster.GetEmployee(id);
  var detailObj = employee?.Details(detail);

  if(detailObj && detailObj instanceof Date)
  { // Since we can only return primitive types, we have to convert the Date object to a primitive JS object
    var dateObj = detailObj as Date;

    ret = {
      "Year":     dateObj.getFullYear(),
      "Month":    dateObj.getMonth(),
      "Date":     dateObj.getDate(),
      "Day":      dateObj.getDay(),
      "Hours":    dateObj.getHours(),
      "Minutes":  dateObj.getMinutes(),
      "Seconds":  dateObj.getSeconds(),
    };
  }
  else
    ret = detailObj == undefined ? "undefined" : detailObj;

  return ret;
}

/**
 * Load the roster and create a calendar,
 * which will be associated to an employee,
 * and return its calendar ID
 * 
 * @param id The ID of the employee, for whom the calendar will be created
 * @returns The newly created calendar's ID
 */
function py_createCalendar(id: CellValue): string
{
  var main = entryPoint();
  var employee = main.Roster.GetEmployee(id);
  var calendarId = /*employee?.Schedule.CreateCalendar().getId() ??*/ ""; // FIXME: https://stackoverflow.com/a/34586

  return calendarId;
}

function py_generateSchedule(id: CellValue): string
{
  var main = entryPoint();
  var employee = main.Roster.GetEmployee(id);
  var nowDate = new Date(),
      startDate = new Date(nowDate.getFullYear(), nowDate.getMonth(), 1),
      endDate = new Date(nowDate.getFullYear(), nowDate.getMonth(), 30);

  employee?.Schedule.Generate(startDate, endDate); // FIXME: This should return the calendarId

  return "";
}