// roster.ts
/**
 * Roster model controller
 * 
 * @module
 */

const RosterSheetName: string = "Roster";

/**  Roster Sheet controller */
class RosterSheet extends Sheet
{
  private employees?: Map<CellValue, Employee>; // Initialised and populated in base class

  /**
   * NOTE: The value of the `rosterData` parameter is modified here
   * 
   * @param rosterData `[Out]` The roster data extracted from the sheet
   */
  constructor(rosterData: Map<CellValue, Employee>)
  {
    super(RosterSheetName, [
      new Cell<string>("ID", 1, 1),
      new Cell<string>("Name", 1, 2),
    ]);

    if(this.employees)
      for(let [employeeId, employee] of this.employees)
        rosterData.set(employeeId, employee);
  }

  private initEmployees(rosterDetails: Map<CellValue, Map<string, Cell>>): void
  {
    var builder = new EmployeeBuilder();

    this.employees = new Map<CellValue, Employee>();

    for (let [employeeId, employeeDetails] of rosterDetails)
    {
      var employee = builder.Build(employeeDetails);
      this.employees.set(employeeId, employee);
    }
  }

  protected LoadData(dataRange: Map<[number, number], Cell>): void
  {
    var rosterDetails = new Map<CellValue, Map<string, Cell>>(); // [employeeId, [detail, value]]

    var rowNum, colNum;

    for(var [dims, cell] of dataRange)
    {
      [rowNum, colNum] = [dims[0], dims[1]];
      var employeeId: CellValue = "";

      var detail: string = dataRange.get([1, colNum])?.Val<string>() ?? Cell.Blank().Val();

      if(employeeId.toString() === "" && detail === "ID")
      {
        employeeId = cell.Val();

        if(employeeId === "")
        {
          console.log(this.constructor.name + "(): Empty employee ID on [" + rowNum + ", " + colNum + "]");
          break;
        }

        rosterDetails.set(employeeId, new Map<string, Cell>());
        colNum = 0; // reset the loop, in case the ID column wasn't the first one, in which case we would miss to record those preceding it
      }
      else
      {
        rosterDetails.get(employeeId)?.set(detail, cell);
      }
    }

    this.initEmployees(rosterDetails);
  }
}

/** [Singleton](https://en.wikipedia.org/wiki/Singleton_pattern) class for access to the roster */
class Roster
{
  private sheet: Sheet;
  private readonly roster: Map<CellValue, Employee>; // [id, employee]
  private static singleton: Roster;

  public static get Singleton()
  {
    if(Roster.singleton == undefined)
      Roster.singleton = new Roster();

    return Roster.singleton;
  }

  private constructor()
  {
    this.roster = new Map<CellValue, Employee>();
    this.sheet = new RosterSheet(this.roster);
  }

  /** Get the entire roster of employees */
  public get GetRoster() { return this.roster; }

  /**
   * @param id The employee ID
   * @returns The employee object, or null if the employee was not found
   */
  public GetEmployee(id: CellValue): Employee | null
  {
    return this.roster.get(id) ?? null;
  }

  /**
   * @param detailCell The cell containing the employee detail
   * @returns True if the cell was successfully written onto the sheet
   */
  public UpdateEmployeeDetail(detailCell: Cell): boolean
  {
    if(!this.sheet.SetData(new Array<Cell>(detailCell)))
    {
      console.log(this.constructor.name + "(): Failed to update employee detail");
      return false;
    }

    return true;
  }
}