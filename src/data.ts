// data.ts
/**
 * Handles all spreadsheet operations (data I/O, sheet creation)
 * 
 * @module
 */

/** [Singleton](https://en.wikipedia.org/wiki/Singleton_pattern) [Facade](https://en.wikipedia.org/wiki/Facade_pattern)
 * class for all spreadsheet operations (data I/O, sheet creation) */
class Data
{
  private spreadsheet: gSpreadsheet;
  private static singleton: Data

  private constructor()
  {
    this.spreadsheet = SpreadsheetApp.getActiveSpreadsheet();
  }

  public static get Singleton()
  {
    if(Data.singleton == undefined)
      Data.singleton = new Data();

    return Data.singleton;
  }

  /**
  * Create a new sheet
  * 
  * @param sheetName The name of the sheet
  */
  public Create(sheetName: string): gSheet
  {
    return this.spreadsheet.insertSheet(sheetName);
  }

  /**
  * @param sheetName The name of the sheet
  * @returns The sheet's full data range, indexed by `[row, column]`; `null` if no sheet by that name is found in the active spreadsheet, or if the sheet is composed entirely out of empty cells
  */
  public Get(sheetName: string): [gSheet, Map<[number, number], Cell>] | null;
  /**
  * @param sheet Sheet object
  * @returns The sheet's full data range, indexed by `[row, column]`; `null` if no sheet by that name is found in the active spreadsheet, or if the sheet is composed entirely out of empty cells
  */
  public Get(sheet: gSheet): Map<[number, number], Cell> | null;
  
  // https://stackoverflow.com/a/12689054
  public Get(sheetArg: gSheet | string): Map<[number, number], Cell> | [gSheet, Map<[number, number], Cell>] | null 
  {
    var ret: Map<[number, number], Cell> = new Map<[number, number], Cell>();

    var sheet: gSheet | null = typeof sheetArg === "string" ? this.spreadsheet.getSheetByName(sheetArg) : sheetArg;
    var dataRange: gRange = sheet?.getDataRange() ?? null;
    var dataValues = dataRange?.getValues();
    var row: number, column: number;

    if(dataValues != undefined)
    {
      for(var rowId in dataValues)
      {
        for(var colId in dataValues[rowId])
        {
          [row, column] = [dataValues.indexOf(dataValues[rowId])+1, dataValues[rowId].indexOf(dataValues[rowId][colId])+1];
          ret.set([row, column], new Cell(dataValues[rowId][colId], row, column));
        }
      }
    }

    return ret;
  }

  /**
  * @param sheetName The name of the sheet
  * @param row Row ID (starts from 1)
  * @param column Column ID (starts from 1)
  * @param value New value
  * @returns True if the value was successfully set on the cell. False if there was no sheet found by that name, or if the cell's row or column were invalid
  */
  public Set(sheetName: string, row: number, column: number, value: CellValue): boolean;
  /**
  * @param sheet Sheet object
  * @param row Row ID (starts from 1)
  * @param column Column ID (starts from 1)
  * @param value New value
  * @returns True if the value was successfully set on the cell. False if the cell's row or column were invalid
  */
  public Set(sheet: gSheet, row: number, column: number, value: CellValue): boolean;
  /**
  * @param sheet Sheet object
  * @param cell Cell object, with populated `row` and `column`
  * @returns True if the value was successfully set on the cell. False if the cell's row or column were invalid
  */
  public Set(sheet: gSheet, cell: Cell): boolean;

  public Set(sheetArg: gSheet | string, rowOrCell: number | Cell, column?: number, value?: CellValue): boolean
  {
    var sheet: gSheet | null = typeof sheetArg === "string" ? this.spreadsheet.getSheetByName(sheetArg) : sheetArg;
    var rowN: number = 0, columnN: number = 0;
    var ret: boolean;
    
    if(typeof rowOrCell === "number" && column != undefined)
      [rowN, columnN] = [rowOrCell, column];
    else if(rowOrCell instanceof Cell)
      [rowN, columnN] = [rowOrCell.Row, rowOrCell.Column];
    
    if(sheet == null)
    {
      console.log(this.constructor.name + "(): No sheet by name '" + sheetArg + "' was found.");
      ret = false;
    }
    else if(rowN < 1 || columnN < 1)
    {
      console.log(this.constructor.name + "(): Invalid row or column.");
      ret = false;
    }
    else
    {
      sheet.getRange(rowN, columnN).
            setValue(value);
      ret = true;
    }

    return ret;
  }
}