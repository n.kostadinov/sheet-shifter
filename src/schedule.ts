// schedule.ts
/**
 * Schedule model controller.
 * 
 * @module
 */

/**
 * Schedule Sheet controller
 */
class ScheduleSheet extends Sheet
{
  private scheduleData?: ScheduleDay[]; // Initialised and populated in base class
  
  /**
   * @param date The date period (month & year) of the schedule
   * @param scheduleData The schedule data extracted from the sheet
   */
  constructor(date: Date, scheduleData: ScheduleDay[])
  {
    var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    var sheetName = months[date.getMonth()] + " '" + (date.getFullYear() % 1000);
  
    super(sheetName, ScheduleSheet.SetupHeadData(date));
    
    if(this.scheduleData)
      for(var day in scheduleData)
        scheduleData.push(scheduleData[day]);
  }

  /**
   * 
   * @param date The date period (month & year) of the schedule
   * @returns The header cells for the provided date period's schedule
   */
  private static SetupHeadData(date: Date): Cell[]
  {
    var staticHeaders: Cell[] = [
      new Cell<string>("[ID] Name", 1, 1),
    ];
    var roster: Map<CellValue, Employee> = Roster.Singleton.GetRoster;
    var row = 2;

    for(var id in roster)
    {
      var employee = roster.get(id);
      var name = employee?.Name;
      staticHeaders.push(new Cell("[" + id + "] " + (name ?? ""), row++, 1));
    }

    var lastDate: number = new Date(date.getFullYear(), date.getMonth()+1, 0).getDate();
    for(var i=1; i<lastDate; i++)
    {
      staticHeaders.push(new Cell<number>(i, 1, i+1));
    }

    return staticHeaders;
  }
  
  protected LoadData(dataRange: Map<[number, number], Cell>): void
  {
    var rowNum, colNum;

    for(var [dims, cell] of dataRange)
    {
      [rowNum, colNum] = [dims[0], dims[1]];
      var cellValue = cell.Val();
      var day: ScheduleDay = {};

      if(cellValue instanceof Date)
      {
        var belowCellValue = dataRange.get([rowNum+1, colNum])?.Val();
        day.type = ScheduleDayType.WORKING;
        day.startTime = cellValue;

        if(belowCellValue instanceof Date)
        {
          day.endTime = belowCellValue;
        }
        else
        {
          console.log(this.constructor.name + "(): Working day with no end on [" + rowNum + ", " + colNum + "]");
          day.endTime = cellValue; // Populating data to avoid crashes down the line
        }
      }
      else if(typeof cellValue === "string")
      {
        day.type = ScheduleDayTypes.indexOf(cellValue)

        if(day.type == ScheduleDayType.INVALID)
          console.log(this.constructor.name + "(): Unrecognised schedule day type '" + cellValue + "' on [" + rowNum + ", " + colNum + "]");
      }
      else
      {
        console.log(this.constructor.name + "(): Unrecognised input '" + cellValue + "' on [" + rowNum + ", " + colNum + "]");
      }
    }
  }
}

/** An enum representing the named colors available in the Calendar service.
 * [Link to documentation.](https://developers.google.com/apps-script/reference/calendar/color) */
enum Color
{
  BLUE,
  BROWN,
  CHARCOAL,
  CHESTNUT,
  GRAY,
  GREEN,
  INDIGO,
  LIME,
  MUSTARD,
  OLIVE,
  ORANGE,
  PINK,
  PLUM,
  PURPLE,
  RED,
  RED_ORANGE,
  SEA_BLUE,
  SLATE,
  TEAL,
  TURQOISE,
  YELLOW,
}

/** Specifies advanced parameters for calendar creation,
 * as listed [here](https://developers.google.com/apps-script/reference/calendar/calendar-app#advanced-parameters_3). */
interface CalendarOptions
{
  /** The calendar's location.
   * @example "136 Edinburgh Ave, Slough SL1 4SS, UK"*/ 
  location?: string;

  /** The calendar's description.
   * @example "Building 120" */
  summary?: string;

  /** The time zone to set the calendar to, specified in "long" format, as listed by Joda.org
   * @example "Europe/London" */
  timeZone?: string;

  /** A hexadecimal color string ("#rrggbb") or a value from the `Color` enum
   * @example CalendarApp.Color.GREEN */
  color?: Color | string;

  /** Whether the calendar is hidden in the user interface (default: false)
   * @example false */
  hidden?: boolean;

  /** Whether the calendar's events are displayed in the user interface (default: true)
   * @example true */
  selected?: boolean;
}

/** Specifies advanced parameters for event creation,
 * as listed [here](https://developers.google.com/apps-script/reference/calendar/calendar-app#advanced-parameters_5). */
interface EventOptions
{
  /** The description of the events in the series */
  description?: string;
  
  /** The location of the events in the series */
  location?: string;
  
  /** A comma-separated list of email addresses that should be added as guests to the events in the series */
  guests?: string;
  
  /** Whether to send invitation emails (default: false) */
  sendInvites?: boolean;
}

/** Schedule day types */
enum ScheduleDayType
{
  /** @ignore */
  INVALID = -1,
  /** Day off */
  DO,
  /** Sick leave */
  SL,
  /** Paid leave */
  PL,
  /** Unpaid leave */
  UPL,
  /** Working day*/
  WORKING,
}

/** Schedule day types presented as strings */
const ScheduleDayTypes: string[] = [
  "Day Off",
  "Sick Leave",
  "Paid Leave",
  "Unpaid Leave",
  "Working day"
];

/** An individual calendar day from the schedule. */
interface ScheduleDay
{
  type?: ScheduleDayType;

  startTime?: Date;

  endTime?: Date | null;
}

/** Schedule class. Data consists of a collection of ScheduleDay objects*/
class Schedule
{
  private readonly schedule: ScheduleDay[];
  private static holidays: gEvent[];

  constructor(employee: Employee)
  {
    this.schedule = [];
    var currentDate: Date = new Date();
    var sheet: ScheduleSheet = new ScheduleSheet(currentDate, this.schedule);
    var calendarId = employee.CalendarID;
    var calendar: gCalendar;

    if(calendarId === undefined)
    {
      var calendarOptions: CalendarOptions = {
        color: Color.PURPLE,
      };

      calendar = CalendarApp.createCalendar("gScheduler Work Schedule", calendarOptions);

      employee.Set("Calendar ID", calendar.getId());
    }
    else
    {
      calendar = CalendarApp.getCalendarById(calendarId);
    }

    if(sheet.NewSheet)
    {
      this.CreateEvents(currentDate, calendar,
        employee.PrefStart,
        employee.PrefEnd,
        employee.WorkingDays,
        employee.WorkingHolidays);
    }
    else
    {
      this.VerifyEventChanges(currentDate, calendar);
    }
  }

  /**
   * Creates two event sets for working and off days. A day is considered to be a working day if it's either:
   * 
   *  - A day of the week, which is marked as working in the employee's details, and is NOT a holiday
   *  - A holiday, but the employee works holidays, based on their details
   * 
   * @param date The date period (month & year) of the schedule
   * @param calendar The calendar to create the events in
   * @param shiftStart Shift start time (hours & minutes)
   * @param shiftEnd Shift end time (hours & minutes)
   * @param workingWeekDays The current employee's workdays presented a number with bitflags, suitable for comparisson with Date().getDay()
   * @param workingHolidays True if the current employee works holidays
   */
  public CreateEvents(date: Date, calendar: gCalendar, shiftStart: Date, shiftEnd: Date, workingWeekDays: number, workingHolidays: boolean): void
  {
    var endDay: number = new Date(date.getFullYear(), date.getMonth()+1, 0).getDate();
    var startTime: Date = new Date(date.getFullYear(), date.getMonth(), date.getDate(), shiftStart.getHours(), shiftStart.getMinutes()),
        endTime: Date = new Date(date.getFullYear(), date.getMonth(), date.getDate(), shiftEnd.getHours(), shiftEnd.getMinutes());
    var workingDays = new Array<number>(),
        offDays = new Array<number>();

    for(var i=0; i<endDay; i++)
    {
      var currDate: Date = new Date(date.getFullYear(), date.getMonth(), i+1);
      var currDay: number = currDate.getDate();
      var currWeekDay: number = currDate.getDay();
      var todayWorkday: boolean = (1 << currWeekDay) === (workingWeekDays & (1 << currWeekDay));
      var todayHoliday: boolean = Schedule.isDateHoliday(currDate);

      if(todayWorkday && (!todayHoliday || (todayHoliday && workingHolidays)))
        if(workingDays.indexOf(currDay) == -1)
          workingDays.push(currDay);
      else
        if(offDays.indexOf(currDay) == -1)
          offDays.push(currDay);
    }

    workingDays.sort((a,b) => a-b);
    offDays.sort((a,b) => a-b);

    if(workingDays.length > 0)
    {
      var workingDayOptions: EventOptions = {
        sendInvites: true,
      };

      calendar.createEventSeries(
        ScheduleDayTypes[ScheduleDayType.WORKING],
        startTime, endTime, 
        CalendarApp.newRecurrence().
                    addMonthlyRule().
                    onlyOnMonthDays(workingDays),
        workingDayOptions);
    }
    
    if(offDays.length > 0)
    { // Google pls fix: https://issuetracker.google.com/issues/112063903
      var dayOffOptions: EventOptions = {
      };

      calendar.createAllDayEventSeries(
        ScheduleDayTypes[ScheduleDayType.DO],
        startTime,
        CalendarApp.newRecurrence().
                    addMonthlyRule().
                    onlyOnMonthDays(offDays),
        dayOffOptions);
    }
  }

  /**
   * Verify if any changes have been made to the schedule by the employee, and whether they're legal
   * 
   * @param date The date period (month & year) of the schedule
   * @param calendar The calendar, the events of which to compare against the records from the spreadsheet
   */
  public VerifyEventChanges(date: Date, calendar: gCalendar): void
  {
    var startDate: Date = new Date(date.getFullYear(), date.getMonth(), date.getDate()),
        endDate: Date = new Date(date.getFullYear(), date.getMonth(), date.getDate());
    var events: gEvent[] = calendar.getEvents(startDate, endDate)

    for(var evIdx in events)
    {
      var event = events[evIdx];
      var typeStr = event.getTitle();
      var type: ScheduleDayType = ScheduleDayTypes.indexOf(typeStr);
      var startTime = event.getStartTime(),
          endTime = event.getEndTime();
      var activeDay = startTime.getDate(); // Shifts count toward the day they start from, not the day they end (for late and night shifts)
      var scheduleDay = this.schedule?.[activeDay];

      if(scheduleDay == undefined)
      {
        if(type != ScheduleDayType.INVALID)
        {
          this.schedule.push({
            type: type,
            startTime: new Date(startTime.getUTCMilliseconds()),
            endTime: type == ScheduleDayType.WORKING ? new Date(endTime.getUTCMilliseconds()) : null,
          });
        }
        else
        {
          console.log(this.constructor.name + "(): Invalid event type '" + typeStr + "'");
        }
      }
      else if(scheduleDay.type == ScheduleDayType.WORKING)
      {
        scheduleDay.endTime = new Date(endTime.getUTCMilliseconds());
      }
    }
  }

  /** Compile a list of holidays for this schedule's date period (month & year) */
  private static getHolidays()
  {
    Schedule.holidays = new Array<gEvent>();

    // Events, which are present in the Google Bulgarian Holiday calendar, but are not official bank holidays
    const nonHolidays: string[] = [
      'Day of Remembrance and Respect to Victims of the Communist Regime',
      'Baba Marta',
      'Daylight Saving Time starts',
      'July Morning',
      'Daylight Saving Time ends',
      'Revival Day'
    ];
    
    var now = new Date(),
        startDate = new Date(now.getFullYear(), now.getMonth(), 1),
        endDate = new Date(now.getFullYear(), now.getMonth()+1, 0);
    var calendar: gCalendar = CalendarApp.getCalendarById("en.bulgarian#holiday@group.v.calendar.google.com");
    var events: gEvent[] = calendar.getEvents(startDate, endDate);

    for(var evKey in events)
    {
      var event: gEvent = events[evKey];

      for(var hKey in nonHolidays)
      {
        if(event.getTitle() == nonHolidays[hKey])
          continue;
        else
          Schedule.holidays.push(event);
      }
    }
  }

  /** @returns True if `date` is a holiday */
  private static isDateHoliday(date: Date): boolean
  {
    if(Schedule.holidays != undefined)
      Schedule.getHolidays();

    var ret: boolean = false;

    for(var evKey in Schedule.holidays)
    {
      var event: gEvent = this.holidays[evKey];
      var eventDate: gDate = event.getStartTime();

      if( eventDate.getFullYear() == date.getFullYear() &&
          eventDate.getMonth()    == date.getMonth() &&
          eventDate.getDate()     == date.getDate())
      {
        ret = true;
        break;
      }
    }

    return ret;
  }
}