// sheets.ts
/**
 * Handles the management of all sheets and structuring their data into useable models.
 * 
 * @module
 */

/** [Composite](https://en.wikipedia.org/wiki/Composite_pattern) class for setting up all operational sheets */
abstract class Sheet
{
  private sheetName: string;
  private sheet: gSheet;
  private newSheet: boolean;

  /** @returns True if the sheet was created during the current execution */
  public get NewSheet() { return this.newSheet; }

  /**
   * @param sheetName The name of the sheet. Used to read, write & validate its data
   * @param staticHeaders Static header cells, such as value indicators, column headers, etc.
   */
  constructor(sheetName: string, staticHeaders: Cell[])
  {
    var sheetGet: [gSheet, Map<[number, number], Cell>] | null = Data.Singleton.Get(sheetName);
    var dataRange: Map<[number, number], Cell>;
    var headOk: boolean;

    this.sheetName = sheetName;

    if(sheetGet == null)
    {
      this.sheet = Data.Singleton.Create(sheetName);
      this.newSheet = true;

      headOk = this.InitHead(staticHeaders);

      // Directly clear erroneous data, or try to load it, despite the wrong formatting? Hmm...
    }
    else
    {
      this.sheet = sheetGet[0];
      this.newSheet = false;
      dataRange = sheetGet[1];
      
      headOk = !this.VerifyHead(dataRange, staticHeaders) ? this.InitHead(staticHeaders) : true;

      if(!headOk)
      {
        console.log(this.constructor.name + ": Failed to setup columns of sheet '" + sheetName + "'");
      }
      else
      {
        this.LoadData(dataRange);
      }
    }
  }

  /**
   * Sets up the header data necessary for the functioning of the sheet
   * 
   * @param staticHeaders Static header cells, such as value indicators, column headers, etc.
   * @returns True if all header cells have their values successfully set on the sheet
   */
  private InitHead(staticHeaders: Cell[]): boolean
  {
    var ret: boolean = true;

    for(var i=0; i<staticHeaders.length; i++)
      ret = ret && Data.Singleton.Set(this.sheet, staticHeaders[i]);

    return ret;
  }
  
  /**
   * Verifies the header data of this sheet
   * 
   * @param dataRange The entire data range of the sheet
   * @param staticHeaders Static header cells, such as value indicators, column headers, etc.
   * @returns True if the cell data provided matches the data from the sheet
   */
  protected VerifyHead(dataRange: Map<[number, number], Cell>, staticHeaders: Cell[]): boolean
  {
    var ret: boolean = true;

    for(var headerIdx in staticHeaders)
    {
      var [row, column]: number[] = [staticHeaders[headerIdx].Row, staticHeaders[headerIdx].Column];
      var headData: CellValue = staticHeaders[headerIdx].Val();
      var cellData: CellValue = dataRange.get([row, column])?.Val() ?? Cell.Blank().Val();

      ret = ret && (cellData === headData);
    }
    
    return ret;
  }

  /** Format the validated sheet data into useable data structures */
  protected abstract LoadData(dataRange: Map<[number, number], Cell>): void;

  /**
   * @param data The cells to update
   * @returns True if all cells were successfully updated
   */
  public SetData(data: Cell[]): boolean
  {
    for(var i=0; i<data.length; i++)
    {
      if(!Data.Singleton.Set(this.sheet, data[i]))
      {
        console.log(this.constructor.name + "(): Failed to set data at [" + data[i].Row + ", " + data[i].Column + "] ' in sheet '" + this.sheetName + "'");
        return false;
      }
    }

    return true;
  }
}