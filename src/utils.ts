// utils.ts
/**
 * Utility defines.
 * 
 * @module
 */

type gDate =        GoogleAppsScript.Base.Date;
type gSpreadsheet = GoogleAppsScript.Spreadsheet.Spreadsheet;
type gSheet =       GoogleAppsScript.Spreadsheet.Sheet | null;
type gRange =       GoogleAppsScript.Spreadsheet.Range | null;
type gCalendar =    GoogleAppsScript.Calendar.Calendar;
type gEvent =       GoogleAppsScript.Calendar.CalendarEvent;

/** A type for [all acceptable cell values](https://developers.google.com/apps-script/reference/spreadsheet/range#getvalue).
 * Modified to use primitives for easier integration with the Apps Script API */
type CellValue =    number | boolean | Date | string;

/** A generic class for handling all possible cell values. 
 * @template T The value type depending on the value of the cell. Accepted: ``number | boolean | Date | string`` */
class Cell<T extends CellValue = CellValue>
{
  private type: string;
  private val: T;
  private row: number;
  private column: number;

  /** The cell's type */
  public Type = (): string => this.type;

  /** The cell's value
   * @template ExType Expected cell type
   * @param t Used to extract the cell type to a value. Leave blank
  */
  public Val = <ExType extends T = T>(t: ExType = <ExType>this.val): ExType => typeof t === this.type ? <ExType><unknown>this.val : <ExType>0;

  /** The cell's row in the sheet (starts at 1) */
  public get Row() { return this.row };
  
  /** The cell's column in the sheet (starts at 1) */
  public get Column() { return this.column };

  /** A blank cell */
  public static Blank = (): Cell => new Cell("");

  /**
   * @param cellValue The value of the cell
   * @param row The cell's row in the sheet (starts at 1)
   * @param column The cell's column in the sheet (starts at 1)
   */
  constructor(cellValue: T, row?: number, column?: number)
  {
    this.type = typeof cellValue;

    if( this.type === "number" ||
        this.type === "boolean" ||
        this.type === "string")
      this.val = cellValue;
    else if(cellValue instanceof Date)
      this.val = <T><unknown>(new Date(cellValue));
    else
      this.val = <T><unknown>null; // should never reach here

    this.row = row ?? 0;
    this.column = column ?? 0;
  }
}