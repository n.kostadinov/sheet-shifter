from pythonTestingModule import AppsScript

def test_employeeName():
    # Define the parameters required to run the 'py_getEmployeeDetails' function,
    # as detailed in the docs page of the 'pythonExports' Module
    id = 1

    # Run our remote function and extract its result
    result = AppsScript.scripts.run("py_getEmployeeDetails", id, "Name")

    # Define a variable to verify whether we received a valid response, or if an error was encountered
    validResult = result is not None

    # Test if the below operation is true or false
    assert validResult and result == "BoJack Horseman"

def test_employeeWorkingDays():
    result = AppsScript.scripts.run("py_getEmployeeDetails", 1)

    assert result["Working days"] == [0, 1, 2, 3, 4, 5, 6]

def test_employeePrefStartTime():
    result = AppsScript.scripts.run("py_getEmployeeDetail", 1, "Pref. start")

    year =      result["Year"]
    month =     result["Month"]
    date =      result["Date"]
    day =       result["Day"]
    hours =     result["Hours"]
    minutes =   result["Minutes"]
    seconds =   result["Seconds"]

    assert hours == 10 and minutes == 0 and seconds == 0