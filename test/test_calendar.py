from pythonTestingModule import AppsScript, Calendar

calendarId = ""

def test_createCalendar():
    global calendarId

    calendarId = AppsScript.scripts.run("py_createCalendar", 1)

    assert calendarId != ""


def test_verifyCalendarDetails():
    calendar = Calendar.CalendarList.get(calendarId)

    # TODO:
    # details = AppsScript.scripts.run("py_getEmployeeDetails", 1)
    # Get a list of events between the date range & iterate through them
    # Check if each calendar's name fits the "Work" / "Day Off" frame
    # when compared to the employee's 'Working days' and 'Working holidays' details
    # TODO: add Holidays.Instance.IsDateHoliday() to pyExports

    location    = calendar.location
    summary     = calendar.summary # This is actually the calendar's name, not its summary
    timeZone    = calendar.timeZone
    color       = calendar.colorId # e.g '8'
    selected    = calendar.selected

    assert location    == "136 Edinburgh Ave, Slough SL1 4SS, UK"
    assert summary     == "Temp" #"Building 120"
    assert timeZone    == "Europe/London"
    assert color       == Calendar.Colors.get().calendar[color] #CalendarApp.Color.GREEN
    assert selected    == True

def test_deleteCalendar():
    calendarDeleted = Calendar.CalendarList.delete(calendarId)

    assert calendarDeleted == None
    