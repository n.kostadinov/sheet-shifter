
from __future__ import annotations
from .main import Service, Resource, Object, SubObject, Method
import datetime

class Value(SubObject):
    @property
    def background(self) -> str:
        """The background color associated with this color definition."""
        return self._background

    @property
    def foreground(self) -> str:
        """The foreground color that can be used to write on top of a background with 'background' color."""
        return self._foreground

class Key(SubObject):
    @property
    def key(self) -> Key:
        """An event color definition."""
        return Key(self._key)

class Calendar(SubObject):
    @property
    def key(self) -> Key:
        """A calendar color definition."""
        return Key(self._key)

class Colors(Resource, Object):
    """`Link to documentation <https://developers.google.com/calendar/v3/reference/colors>_`"""
    @property
    def calendar(self) -> dict[Key, Value]:
        """A global palette of calendar colors, mapping from the color ID to its definition.
        A calendarListEntry resource refers to one of these color IDs in its colorId field.
        """
        return dict[Key, Value](self._calendar)

    @property
    def event(self) -> dict[Key, Value]:
        """A global palette of event colors, mapping from the color ID to its definition.
        An event resource may refer to one of these color IDs in its colorId field.
        """
        return dict[Key, Value](self._event)

    @property
    def kind(self) -> str:
        """Type of the resource ("calendar#colors")."""
        return self._kind

    @property
    def updated(self) -> datetime:
        """Last modification time of the color palette (as a RFC3339 timestamp)."""
        return datetime(self._updated)

    def __init__(self, service: Service = None, jsonDic: dict = None) -> None:
        # https://stackoverflow.com/a/26927718
        if(jsonDic is not None):
            Object.__init__(self, jsonDic=jsonDic)
        elif(service is not None):
            Resource.__init__(self, service=service)

    @Method
    def get(self) -> Colors:
        """Returns the color definitions for calendars and events.

        `Link to documentation <https://developers.google.com/calendar/v3/reference/colors/get>`_
        """
        jsonResponse =  self.service.apiResource.colors().get().execute()

        colors = Colors(jsonResponse)

        return colors