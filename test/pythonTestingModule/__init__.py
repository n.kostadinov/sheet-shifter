
from .appsscript import AppsScript as AppsScriptService # AppsScript service
from .calendar import Calendar as CalendarService # Calendar service

AppsScript = AppsScriptService('script', 'v1')
Calendar = CalendarService('calendar', 'v3')

__all__ = [
    'AppsScript',
    'Scripts',
    ]