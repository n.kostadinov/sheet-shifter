
from __future__ import annotations
from .main import Service, Resource, Object, SubObject, Method

class ConferenceProperties(SubObject):
    @property
    def allowedConferenceSolutionTypes(self) -> list[str]:
        """The types of conference solutions that are supported for this calendar.
        The possible values are:

        - `"eventHangout"`
        - `"eventNamedHangout"`
        - `"hangoutsMeet"`

        Optional.
        """
        return list[str](self._allowedConferenceSolutionTypes)

class DefaultReminder(SubObject):
    @property
    def method(self) -> str:
        """The method used by this reminder. Possible values are:
        - `"email"` - Reminders are sent via email.
        - `"popup"` - Reminders are sent via a UI popup.
        
        Required when adding a reminder.
        """
        return self._method

    @property
    def minutes(self) -> int:
        """Number of minutes before the start of the event when the reminder should trigger.
        Valid values are between 0 and 40320 (4 weeks in minutes).

        Required when adding a reminder.
        """
        return self._minutes

class Notification(SubObject):
    @property
    def method(self) -> str:
        """The method used to deliver the notification. The possible value is:
        - `"email"` - Notifications are sent via email.

        Required when adding a notification.
        """
        return self._method

    @property
    def type(self) -> str:
        """The type of notification. Possible values are:

        - `"eventCreation"` - Notification sent when a new event is put on the calendar.
        - `"eventChange"` - Notification sent when an event is changed.
        - `"eventCancellation"` - Notification sent when an event is cancelled.
        - `"eventResponse"` - Notification sent when an attendee responds to the event invitation.
        - `"agenda"` - An agenda with the events of the day (sent out in the morning).

        Required when adding a notification.
        """
        return self._type

class NotificationSettings(SubObject):
    @property
    def notifications(self) -> list[Notification]:
        """The list of notifications set for this calendar.	"""
        return list[Notification](self._notifications)

class CalendarList(Resource, Object):
    """The collection of calendars in the user's calendar list.

    `Link to documentation <https://developers.google.com/calendar/v3/reference/calendarList>`_
    """
    @property
    def accessRole(self) -> str:
        """The effective access role that the authenticated user has on the calendar. Read-only. Possible values are:

        - `"freeBusyReader"` - Provides read access to free/busy information.
        - `"reader"` - Provides read access to the calendar. Private events will appear to users with reader access, but event details will be hidden.
        - `"writer"` - Provides read and write access to the calendar. Private events will appear to users with writer access, and event details will be visible.
        - `"owner"` - Provides ownership of the calendar. This role has all of the permissions of the writer role with the additional ability to see and manipulate ACLs.
        """
        return self._accessRole
        
    @property
    def backgroundColor(self) -> str:
        """The main color of the calendar in the hexadecimal format `"#0088aa"`.
        This property supersedes the index-based colorId property.
        To set or change this property, you need to specify colorRgbFormat=true in the parameters of the insert, update and patch methods.

        Optional.
        """
        return self._backgroundColor

    @property
    def colorId(self) -> str:
        """The color of the calendar.
        This is an ID referring to an entry in the calendar section of the colors definition (see the colors endpoint).
        This property is superseded by the backgroundColor and foregroundColor properties and can be ignored when using these properties.
        
        Optional.
        """
        return self._colorId

    @property
    def conferenceProperties(self) -> ConferenceProperties:
        """Conferencing properties for this calendar, for example what types of conferences are allowed."""
        return ConferenceProperties(self._conferenceProperties)

    @property
    def defaultReminders(self) -> list[DefaultReminder]:
        """The default reminders that the authenticated user has for this calendar."""
        return list[DefaultReminder](self._defaultReminders)

    @property
    def deleted(self) -> bool:
        """Whether this calendar list entry has been deleted from the calendar list.
        
        Optional.
        
        The default is False."""
        return self._deleted

    @property
    def description(self) -> str:
        """Description of the calendar.
        
        Optional."""
        return self._description

    @property
    def etag(self) -> str:
        """ETag of the resource."""
        return self._etag

    @property
    def foregroundColor(self) -> str:
        """The foreground color of the calendar in the hexadecimal format `"#ffffff"`.
        This property supersedes the index-based colorId property.
        To set or change this property, you need to specify colorRgbFormat=true in the parameters of the insert, update and patch methods.

        Optional.
        """
        return self._foregroundColor

    @property
    def hidden(self) -> bool:
        """Whether the calendar has been hidden from the list. Optional.
        The attribute is only returned when the calendar is hidden, in which case the value is true."""
        return self._hidden

    @property
    def id(self) -> str:
        """Identifier of the calendar."""
        return self._id

    @property
    def kind(self) -> str:
        """Type of the resource ("calendar#calendarListEntry")."""
        return self._kind

    @property
    def location(self) -> str:
        """Geographic location of the calendar as free-form text.
        
        Optional."""
        return self.location

    @property
    def notificationSettings(self) -> NotificationSettings:
        """The notifications that the authenticated user is receiving for this calendar."""
        return NotificationSettings(self._notificationSettings)

    @property
    def primary(self) -> bool:
        """Whether the calendar is the primary calendar of the authenticated user.

        Optional.

        The default is False."""
        return self._primary

    @property
    def selected(self) -> bool:
        """Whether the calendar content shows up in the calendar UI.
        
        Optional.
        
        The default is False."""
        return self._selected

    @property
    def summary(self) -> str:
        """Title of the calendar."""
        return self._summary

    @property
    def summaryOverride(self) -> str:
        """The summary that the authenticated user has set for this calendar.
        
        Optional."""
        return self._summaryOverride

    @property
    def timeZone(self) -> str:
        """The time zone of the calendar.
        
        Optional."""
        return self._timeZone

    def __init__(self, service: Service = None, jsonDic: dict = None) -> None:
        # https://stackoverflow.com/a/26927718
        if(jsonDic is not None):
            Object.__init__(self, jsonDic=jsonDic)
        elif(service is not None):
            Resource.__init__(self, service=service)

    @Method
    def get(self, calendarId) -> CalendarList:
        """Returns a calendar from the user's calendar list.

        `Link to documentation <https://developers.google.com/calendar/v3/reference/calendarList/get>`_
        """
        jsonResponse = self.service.apiResource.calendarList().get(
            calendarId=calendarId
        ).execute()

        calendarList = CalendarList(jsonDic=jsonResponse)

        return calendarList

    @Method
    def delete(self, calendarId) -> bool:
        """Removes a calendar from the user's calendar list. If successful, this method returns True.

        `Link to documentation <https://developers.google.com/calendar/v3/reference/calendarList/delete>`_
        """
        jsonResponse = self.service.apiResource.calendarList().delete(
            calendarId=calendarId
        ).execute()

        return jsonResponse == None