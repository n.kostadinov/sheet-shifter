
from .main import Service
from .scripts import Scripts # Scripts resource

class AppsScript(Service):
    """`Link to documentation <https://developers.google.com/apps-script/api/reference/rest>`_
    """
    @property
    def scripts(self) -> Scripts:
        return self._scripts

    def __init__(self, serviceName: str, version: str) -> None:
        """Manages and executes Google Apps Script projects.
        """
        super().__init__(serviceName, version)
        self._scripts: Scripts = Scripts(self)