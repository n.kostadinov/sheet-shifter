
# Authorization
import os.path
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials
from google.auth.exceptions import RefreshError
from os import remove
# Service
from googleapiclient.discovery import Resource, build
# Method
from functools import wraps
from googleapiclient import errors
# Object
from json import loads, dumps
from collections import namedtuple

class Authorization:
    scopes = [  'https://www.googleapis.com/auth/spreadsheets', 
                'https://www.googleapis.com/auth/calendar']
    creds = None
    scriptId = None

    def newCreds(cls):
        flow = InstalledAppFlow.from_client_secrets_file('credentials.json', cls.scopes)
        return flow.run_local_server(port=0)

    @classmethod
    def __init__(cls):
        # If we've already obtained our credentials - exit
        if(cls.creds is not None):
            return

        # Define the parameters of our OAuth authentication
        creds = None

        # The file token.json stores the user's access and refresh tokens, and is
        # created automatically when the authorization flow completes for the first time.
        if os.path.exists('token.json'):
            creds = Credentials.from_authorized_user_file('token.json', cls.scopes)

        # If there are no (valid) credentials available, let the user log in.
        if not creds or not creds.valid:
            if creds and creds.expired and creds.refresh_token:
                try:
                    creds.refresh(Request())
                except RefreshError as e:
                    error_details, response_data = e.args

                    # Our access token expired, so delete it and try to login again
                    if (error_details == "invalid_grant: Bad Request"):
                        remove("token.json")
                        creds = cls.newCreds(cls)
            else:
                creds = cls.newCreds(cls)

            # Save the credentials for the next run
            with open('token.json', 'w') as token:
                token.write(creds.to_json())

        # Store this instance of the credentials to be used by all services
        cls.creds = creds

        # Fetch the most recent testing deployment ID
        if os.path.exists('.testingDeploymentId'):
            with open('.testingDeploymentId') as f:
                cls.scriptId = [line.rstrip() for line in f][0]
        else:
            print("No testing deployment found. Please run the command 'npm run build'")

class Service:
    """Build a service object for interacting with an API.
    """

    @property
    def apiResource(self) -> Resource:
        return self._apiResource

    def __init__(self, serviceName: str, version: str) -> None:
        self._apiResource = build(serviceName, version, credentials=Authorization().creds)

    def __del__(self):
        if self._apiResource is not None:
            self._apiResource.close()

class Resource:
    service: Service

    def __init__(self, service: Service = None) -> None:
        if not service:
            pass

        self.service = service

def Method(f):
    @wraps(f)
    def wrapper(*args, **kwds):
        try:
            ret = f(*args, **kwds)
            return ret
        except errors.HttpError as e:
            print('Error response status code : {0}, reason : {1}'.format(e.status_code, e.error_details))

            if(e.status_code == 404 and e.error_details == "Requested entity was not found."):
                print("Try running the build script ('npm run build').")
            
    return wrapper

class Object:
    def object_hook(self, d):
        if "@type" in d:
            d["type_"] = d.pop("@type")
        return namedtuple('obj', d.keys())(*d.values())

    def __init__(self, jsonDic: dict = None):
        if not dict:
            pass

        obj = loads(dumps(jsonDic), object_hook=self.object_hook)

        for attr, value in obj._asdict().items():
            setattr(self, "_"+attr, value)
            
            if attr == "type_" and  value != "type.googleapis.com/google.apps.script.v1." + getattr(self, attr).__class__.__class__:
                print("Attempted to set type '" + value + "' to " + getattr(self, attr).__class__.__class__)
                pass

class SubObject:
    def __init__(self, src: object) -> None:
        if hasattr(src, "type_") and src.type_ != "type.googleapis.com/google.apps.script.v1." + self.__class__.__name__:
            print("Attempted to set type '" + src.type_ + "' to " + self.__class__.__name__)
            pass

        for attr in dir(src):
            if not attr.startswith('__'):
                value = getattr(src, attr)
                setattr(self, "_"+attr, value)