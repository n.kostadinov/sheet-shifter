
from .main import Service
from .calendarlist import CalendarList # CalendarList resource
from .colors import Colors # Colors resource

class Calendar(Service):
    """`Link to documentation <https://developers.google.com/calendar/v3/reference>`_
    """

    @property
    def CalendarList(self) -> CalendarList:
        return self._CalendarList

    @property
    def Colors(self) -> Colors:
        return self._Colors

    def __init__(self, serviceName: str, version: str) -> None:
        """The Calendar API lets you display, create and modify calendar events
        as well as work with many other calendar-related objects,
        such as calendars or access controls.
        """
        super().__init__(serviceName, version)
        self._CalendarList: CalendarList = CalendarList(self)
        self._Colors: Colors = Colors(self)