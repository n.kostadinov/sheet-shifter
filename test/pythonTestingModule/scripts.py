
from .main import Authorization, Resource, Object, SubObject, Method

class ScriptStackTraceElement(SubObject):
    """A stack trace through the script that shows where the execution failed.

    `Link to documentation <https://developers.google.com/apps-script/api/reference/rest/v1/ExecutionError#scriptstacktraceelement>_`
    """
    @property
    def function(self) -> str:
        """The name of the function that failed."""
        return self._function

    @property
    def lineNumber(self) -> int:
        """The line number where the script failed."""
        return self._lineNumber

class ExecutionError(SubObject):
    """An object that provides information about the nature of an error resulting from an attempted execution of a script function using the Apps Script API.
    If a run call succeeds but the script function (or Apps Script itself) throws an exception, the response body's error field contains a Status object.
    The Status object's details field contains an array with a single one of these ExecutionError objects.
    
    `Link to documentation <https://developers.google.com/apps-script/api/reference/rest/v1/ExecutionError>_`
    """
    @property
    def scriptStackTraceElements(self) -> list[ScriptStackTraceElement]:
        """An array of objects that provide a stack trace through the script to show where the execution failed, with the deepest call first."""
        return list[ScriptStackTraceElement](self._scriptStackTraceElements)

    @property
    def errorMessage(self) -> str:
        """The error message thrown by Apps Script, usually localized into the user's language."""
        return self._errorMessage

    @property
    def errorType(self) -> str:
        """The error type, for example TypeError or ReferenceError. If the error type is unavailable, this field is not included."""
        return self._errorType

    @property
    def type_(self) -> str:
        """URI identifying the type of the object. Example: { "id": 1234, "@type": "types.example.com/standard/id" }."""
        return self._type

class Status(SubObject):
    """If a run call succeeds but the script function (or Apps Script itself) throws an exception,
    the response body's error field contains this Status object.

    `Link to documentation <https://developers.google.com/apps-script/api/reference/rest/v1/scripts/run#Status>_`
    """
    @property
    def code(self) -> int:
        """The status code. For this API, this value either:

        - `10`, indicating a SCRIPT_TIMEOUT error,
        - `3`, indicating an INVALID_ARGUMENT error, or
        - `1`, indicating a CANCELLED execution.
        """
        return self._code

    @property
    def message(self) -> str:
        """A developer-facing error message, which is in English.
        Any user-facing error message is localized and sent in the details field, or localized by the client.
        """
        return self._message

    @property
    def details(self) -> list[ExecutionError]:
        """An array that contains a single ExecutionError object that provides information about the nature of the error.
        """
        return list[ExecutionError](self._details)

class ExecutionResponse(SubObject):
    """An object that provides the return value of a function executed using the Apps Script API.
    If the script function returns successfully, the response body's response field contains this ExecutionResponse object.

    `Link to documentation <https://developers.google.com/apps-script/api/reference/rest/v1/ExecutionResponse>_`
    """
    @property
    def result(self) -> any:
        """The return value of the script function. The type matches the object type returned in Apps Script.
        Functions called using the Apps Script API cannot return Apps Script-specific objects (such as a Document or a Calendar);
        they can only return primitive types such as a string, number, array, object, or boolean.
        """
        return self._result

    @property
    def type_(self) -> str:
        """URI identifying the type of the object. Example: { "id": 1234, "@type": "types.example.com/standard/id" }."""
        return self._type_

class Response(Object):
    """`Link to documentation <https://developers.google.com/apps-script/api/reference/rest/v1/scripts/run#response-body>_`
    """
    @property
    def done(self) -> bool:
        """This field indicates whether the script execution has completed.
        A completed execution has a populated response field containing the ExecutionResponse from function that was executed.
        """
        return self._done

    @property
    def error(self) -> Status:
        """If a run call succeeds but the script function (or Apps Script itself) throws an exception, this field contains a Status object.
        The Status object's details field contains an array with a single ExecutionError object that provides information about the nature of the error.
        """
        return Status(self._error)

    @property
    def response(self) -> ExecutionResponse:
        """If the script function returns successfully, this field contains an ExecutionResponse object with the function's return value.
        """
        return ExecutionResponse(self._response)

class Scripts(Resource):
    """`Link to documentation <https://developers.google.com/apps-script/api/reference/rest/v1/scripts>`_
    """
    @Method
    def run(self, functionName: str, *functionArguments) -> ExecutionResponse:
        """Runs a function in an Apps Script project.

        `Link to documentation <https://developers.google.com/apps-script/api/reference/rest/v1/scripts/run>`_
        """
        request = {
            "function": functionName,
            "parameters": []
            }
        
        for arg in functionArguments:
            request["parameters"].append(arg)

        jsonResponse =  self.service.apiResource.scripts().run(
                        body=request,
                        scriptId=Authorization().scriptId
                    ).execute()

        responseBody = Response(jsonResponse)

        if hasattr(responseBody, 'error'):
            details = responseBody.error.details[0]

            print("Running the function '{0}' with arguments {1} resulted in an error: {2}".format(functionName, request["parameters"], details.errorMessage))

            if hasattr(details, 'scriptStackTraceElements'):
                print("Stack trace:")
                for stackTraceElement in details.scriptStackTraceElements:
                    print("\t{0}: {1}".format(stackTraceElement.function, stackTraceElement.lineNumber))
        else:
            return responseBody.response.result