<h1>gScheduler</h3>

<p>
  A workforce management tool built on top of two popular Google services, which function as its employee and manager interfaces.
  <br> 
</p>

## 📝 Table of Contents
- [About](#about)
- [Getting Started](#getting_started)
- [Deployment](#deployment)
- [Usage](#usage)
- [Built Using](#built_using)
- [Authors](#authors)

## 🧐 About<a name = "about"></a>
A WFM tool, which provides a familiar and functional interfaceW for both managers and employees; The former have all the workforce data updated in real-time and available to digest directly in a Google Sheets spreadsheet, thus saving on manual report extraction. The latter have the convenience of having their schedule accessible and editable through any iCal calendar app, device or medium.

This project was mainly started for academic purposes. The main goal was to get exposure to as many technologies and methodologies as possible, while working in a familiar environment.

## 🏁 Getting Started <a name = "getting_started"></a>
These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See [deployment](#deployment) for notes on how to deploy the project on a live system.

### <b>Prerequisites</b>
The following software is required to setup a development environment.

- [Python 3.9+](https://www.python.org/downloads/)
- [pip](https://pypi.org/project/pip/)
- [Node.js and npm](https://nodejs.org/en/)
- [Visual Studio Code](https://code.visualstudio.com/)

### <b>Installing</b>
A step by step guide that tells you how to get a development environment running.

1. Run the below command, or execute the `Install` VSCode task
```
npm install
```
2. Enter your Windows device password when prompted (assuming your account has Administrator privileges)
3. A new browser window or tab will open with Google's OAuth2.0 authentication page. Do what it says.

### <b>Adding members to your environment</b>
Every member of your environment needs credentials in order to access it, including yourself.

1. [Create Desktop application credentials](https://developers.google.com/workspace/guides/create-credentials#desktop)
2. Send the "credentials.json" file to your new member and instruct them to place it in the `/test` folder of their working directory
3. Have youe new member test if everything was installed correctly by building the project with the below command, or by running VSCode's build task (Ctrl+Shift+B)

```
npm run build
```

## 🔧 Running the tests [TODO]<a name = "tests"></a>

```
npm test
```

## 🎈 Usage [TODO]<a name="usage"></a>

## 🚀 Deployment [TODO]<a name = "deployment"></a>

## ⛏️ Built Using <a name = "built_using"></a>
- [Node.js & npm](https://nodejs.org/en/)
- [TypeScript](https://www.typescriptlang.org/)
- [TypeDoc](https://typedoc.org/)
- [Google Cloud](https://cloud.google.com/)
- [Clasp](https://github.com/google/clasp)
- [Python 3.9](https://www.python.org/downloads/)
- [pytest](https://docs.pytest.org/)

## ✍️ Authors <a name = "authors"></a>
- [@n.kostadinov](https://gitlab.com/n.kostadinov) - Idea & Initial work