
@ECHO OFF
SETLOCAL

    REM Update TypeDoc
    CALL npx typedoc

    REM Push any local changes
    CALL npx clasp push -f

    REM Fetch the old testing deployment ID, if any
    FOR /F "delims=" %%A IN ('npx clasp deployments') DO (
        ECHO %%A | FINDSTR /C:"DEV" > NUL && (
            SET OLD_DEPLOYMENT=%%A
        )
    )

    REM Verify if a previous testing deployment exists
    IF DEFINED OLD_DEPLOYMENT (
        GOTO :OLD_DEPLOYMENT_EXISTS
    ) ELSE (
        GOTO :DEPLOY_NEW
    )

    :OLD_DEPLOYMENT_EXISTS
        REM Remove trailing characters
        FOR /F "tokens=1" %%B IN ('ECHO %OLD_DEPLOYMENT:~2%') DO (
            SET OLD_DEPLOYMENT=%%B
        )

        IF NOT EXIST test/.testingDeploymentId (
            GOTO :DEPLOYMENT_FILE_NOT_EXIST
        ) ELSE (
            GOTO :DEPLOY_NEW
        )

    :DEPLOYMENT_FILE_NOT_EXIST
        echo %OLD_DEPLOYMENT% > test/.testingDeploymentId
        GOTO :END

    :DEPLOY_NEW
        REM Undeploy the old testing deployment, provided an old testing deployment ID was found
        CALL npx clasp undeploy %OLD_DEPLOYMENT%

        REM Get the current machine's UUID
        FOR /F "skip=1 delims=" %%C IN ('wmic csproduct get UUID') DO ( 
            IF NOT DEFINED UUID (
                SET UUID=%%C
            )
        )

        REM Deploy the new testing deployment and store its ID
        FOR /F "delims=" %%D IN ('npx -c "clasp deploy -d \"DEV #%DATE% %TIME% #%UUID%\""') DO (
            SET NEW_DEPLOYMENT=%%D
        )

        REM Remove trailing characters & dump the new testing deployment ID to a file
        IF DEFINED NEW_DEPLOYMENT (
            FOR /F "tokens=1" %%E IN ('ECHO %NEW_DEPLOYMENT:~2%') DO (
                ECHO %%E > test/.testingDeploymentId
            )
        ) ELSE (
            ECHO Failed to create new deployment!
        )

    :END
ENDLOCAL