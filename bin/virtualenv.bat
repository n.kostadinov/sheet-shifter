
@ECHO OFF

REM Install the package
pip install virtualenv

REM Setup the environment
CALL virtualenv test

REM Install the Google API Client library
CALL test\Scripts\pip.exe install --upgrade google-api-python-client google-auth-httplib2 google-auth-oauthlib

REM Install pytest
CALL test\Scripts\pip.exe install -U pytest

REM Delete the .gitignore file created by virtualenv
DEL test\.gitignore