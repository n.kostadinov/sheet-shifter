
@ECHO OFF

Rem Navigate to the /test folder. Due to the scale of the testing system, we're keeping all testing-related activities isolated in there
CD test

Rem Activate the Python virtual environment (Not sure if this works in a VSCode task terminal...)
CALL Scripts\\activate.bat

Rem Run pytest
CALL Scripts\\pytest.exe