
@ECHO OFF

REM Setup the virtual Python environment as admin (assuming the currently logged-in user has admin privs)
RUNAS "/user:%USERDOMAIN%\%USERNAME%" "cmd.exe /c cd %cd% && bin\virtualenv.bat"

REM Log into clasp
CALL npx clasp login

REM TODO: npx clasp settings projectId <projectId>